# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Recolector'
        db.create_table('sequias_recolector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('sequias', ['Recolector'])

        # Adding model 'Organizacion'
        db.create_table('sequias_organizacion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('sequias', ['Organizacion'])

        # Adding model 'Entrevistado'
        db.create_table('sequias_entrevistado', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('comunidad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lugar.Comunidad'])),
        ))
        db.send_create_signal('sequias', ['Entrevistado'])

        # Adding model 'Producto'
        db.create_table('sequias_producto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('sequias', ['Producto'])

        # Adding model 'Perdida'
        db.create_table('sequias_perdida', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
        ))
        db.send_create_signal('sequias', ['Perdida'])

        # Adding model 'Primera'
        db.create_table('sequias_primera', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('producto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Producto'])),
            ('planea_siembra', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('area_sembrada', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('area_cosechada', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('produccion', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('perdida', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Perdida'])),
        ))
        db.send_create_signal('sequias', ['Primera'])

        # Adding model 'Postrera'
        db.create_table('sequias_postrera', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('producto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Producto'])),
            ('planea_siembra', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('area_sembrada', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('area_cosechada', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('produccion', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('perdida', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Perdida'])),
        ))
        db.send_create_signal('sequias', ['Postrera'])

        # Adding model 'Apante'
        db.create_table('sequias_apante', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('producto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Producto'])),
            ('planea_siembra', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('area_sembrada', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('area_cosechada', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('produccion', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('perdida', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Perdida'])),
        ))
        db.send_create_signal('sequias', ['Apante'])

        # Adding model 'Disponibilidad'
        db.create_table('sequias_disponibilidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('adultos_casa', self.gf('django.db.models.fields.IntegerField')()),
            ('ninos_casa', self.gf('django.db.models.fields.IntegerField')()),
            ('vacas', self.gf('django.db.models.fields.IntegerField')()),
            ('cerdos', self.gf('django.db.models.fields.IntegerField')()),
            ('gallinas', self.gf('django.db.models.fields.IntegerField')()),
            ('maiz_disponible', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('frijol_disponible', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('sorgo_disponible', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('dinero', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
        ))
        db.send_create_signal('sequias', ['Disponibilidad'])

        # Adding model 'Brazalete'
        db.create_table('sequias_brazalete', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('estado', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
        ))
        db.send_create_signal('sequias', ['Brazalete'])

        # Adding model 'Nutricion'
        db.create_table('sequias_nutricion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('ninos', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('edad', self.gf('django.db.models.fields.IntegerField')()),
            ('brazalete', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Brazalete'])),
        ))
        db.send_create_signal('sequias', ['Nutricion'])

        # Adding model 'Encuesta'
        db.create_table('sequias_encuesta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('colector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Recolector'])),
            ('organizacion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['sequias.Organizacion'])),
        ))
        db.send_create_signal('sequias', ['Encuesta'])


    def backwards(self, orm):
        # Deleting model 'Recolector'
        db.delete_table('sequias_recolector')

        # Deleting model 'Organizacion'
        db.delete_table('sequias_organizacion')

        # Deleting model 'Entrevistado'
        db.delete_table('sequias_entrevistado')

        # Deleting model 'Producto'
        db.delete_table('sequias_producto')

        # Deleting model 'Perdida'
        db.delete_table('sequias_perdida')

        # Deleting model 'Primera'
        db.delete_table('sequias_primera')

        # Deleting model 'Postrera'
        db.delete_table('sequias_postrera')

        # Deleting model 'Apante'
        db.delete_table('sequias_apante')

        # Deleting model 'Disponibilidad'
        db.delete_table('sequias_disponibilidad')

        # Deleting model 'Brazalete'
        db.delete_table('sequias_brazalete')

        # Deleting model 'Nutricion'
        db.delete_table('sequias_nutricion')

        # Deleting model 'Encuesta'
        db.delete_table('sequias_encuesta')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'lugar.comunidad': {
            'Meta': {'object_name': 'Comunidad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'microcuenca': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lugar.Microcuenca']", 'null': 'True', 'blank': 'True'}),
            'municipio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lugar.Municipio']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'lugar.departamento': {
            'Meta': {'object_name': 'Departamento'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'lugar.microcuenca': {
            'Meta': {'object_name': 'Microcuenca'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'lugar.municipio': {
            'Meta': {'object_name': 'Municipio'},
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lugar.Departamento']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'})
        },
        'sequias.apante': {
            'Meta': {'object_name': 'Apante'},
            'area_cosechada': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'area_sembrada': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'perdida': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Perdida']"}),
            'planea_siembra': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'produccion': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Producto']"})
        },
        'sequias.brazalete': {
            'Meta': {'object_name': 'Brazalete'},
            'estado': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'sequias.disponibilidad': {
            'Meta': {'object_name': 'Disponibilidad'},
            'adultos_casa': ('django.db.models.fields.IntegerField', [], {}),
            'cerdos': ('django.db.models.fields.IntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'dinero': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'frijol_disponible': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'gallinas': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maiz_disponible': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'ninos_casa': ('django.db.models.fields.IntegerField', [], {}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'sorgo_disponible': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'vacas': ('django.db.models.fields.IntegerField', [], {})
        },
        'sequias.encuesta': {
            'Meta': {'object_name': 'Encuesta'},
            'colector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Recolector']"}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organizacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Organizacion']"})
        },
        'sequias.entrevistado': {
            'Meta': {'object_name': 'Entrevistado'},
            'comunidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lugar.Comunidad']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        'sequias.nutricion': {
            'Meta': {'object_name': 'Nutricion'},
            'brazalete': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Brazalete']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'edad': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ninos': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'})
        },
        'sequias.organizacion': {
            'Meta': {'object_name': 'Organizacion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'sequias.perdida': {
            'Meta': {'object_name': 'Perdida'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'})
        },
        'sequias.postrera': {
            'Meta': {'object_name': 'Postrera'},
            'area_cosechada': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'area_sembrada': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'perdida': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Perdida']"}),
            'planea_siembra': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'produccion': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Producto']"})
        },
        'sequias.primera': {
            'Meta': {'object_name': 'Primera'},
            'area_cosechada': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'area_sembrada': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'perdida': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Perdida']"}),
            'planea_siembra': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'produccion': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sequias.Producto']"})
        },
        'sequias.producto': {
            'Meta': {'object_name': 'Producto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'sequias.recolector': {
            'Meta': {'object_name': 'Recolector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['sequias']