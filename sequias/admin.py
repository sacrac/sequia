from django.contrib import admin
from lugar.models import Comunidad
from sequias.models import Encuesta, Primera, Postrera, Apante, Disponibilidad, Nutricion, Organizacion, Entrevistado, Producto, Perdida, Recolector, Brazalete
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User

class EntrevistadoInline(generic.GenericStackedInline):
    model = Entrevistado
    extra = 1
    max_num = 1
    
class PrimeraInline(generic.GenericTabularInline):
	model = Primera
	extra = 3
	max_num = 3

class PostreraInline(generic.GenericTabularInline):
	model = Postrera
	extra = 3
	max_num = 3

class ApanteInline(generic.GenericTabularInline):
	model = Apante
	extra = 3
	max_num = 3

class DisponibilidadInline(generic.GenericStackedInline):
	model = Disponibilidad
	extra = 1
	max_num = 1

class NutricionInline(generic.GenericTabularInline):
	model = Nutricion
	extra = 6
	max_num = 6
	
class EncuestaAdmin(admin.ModelAdmin):
#    def queryset(self, request):
#        if request.user.is_superuser:
#            return Encuesta.objects.all()
#        return Encuesta.objects.filter(user=request.user)

#    def get_form(self, request, obj=None, ** kwargs):
#        if request.user.is_superuser:
#            form = super(EncuestaAdmin, self).get_form(self, request, ** kwargs)
#        else:
#            form = super(EncuestaAdmin, self).get_form(self, request, ** kwargs)
#            form.base_fields['user'].queryset = User.objects.filter(pk=request.user.pk)
#        return form
        
	save_on_top = True
	actions_on_top = True
	inlines = [EntrevistadoInline,PrimeraInline,PostreraInline,
	           ApanteInline,DisponibilidadInline,NutricionInline]
	list_display = ['entrevista','comunidades','municipios',]
	list_filter = ['fecha']
	date_hierarchy = 'fecha'
	search_fields = ['entrevistado__nombre']

admin.site.register(Encuesta, EncuestaAdmin)
admin.site.register(Organizacion)
admin.site.register(Producto)
admin.site.register(Perdida)
admin.site.register(Recolector)
admin.site.register(Brazalete)
